import './HomePage.css'; 
import { useNavigate } from 'react-router-dom';
import CNavbar from '../Navbar/CNavbar';

function HomePage() {
  return (
    <div className="home">
      <header>
        <CNavbar />
      </header>
      <main style={{backgroundImage: `url('https://images.unsplash.com/photo-1636955816868-fcb881e57954?q=80&w=2670&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D')`, backgroundSize: 'cover'}}>
      <div className="home-container">
        <h2>Benvenuto nella piattaforma di gestione corsi IT</h2>
        <p>
          Qui puoi trovare una vasta selezione di corsi in ambito IT. Che tu sia
          un principiante o un esperto, c'è qualcosa per tutti!
        </p>
      </div>
      </main>
      <section>
      <h1 className="center-text">I nostri corsi</h1>
      <div className="grid-container">
        <div>
        <h3>Sviluppatore Backend: </h3>
        <p>Impara a costruire e gestire il lato server di un’applicazione web o mobile. Questo corso copre tutto, dai database alla creazione di API RESTful.</p> 
        </div>
        <div>
        <h3>Sviluppatore Frontend: </h3>
        <p>Scopri come creare interfacce utente accattivanti e reattive con HTML, CSS e JavaScript. Questo corso ti guiderà attraverso la creazione di applicazioni web single-page con React.</p>
        </div>
        <div>
        <h3>DevOps:</h3>
        <p>Questo corso è progettato per insegnarti come automatizzare, implementare e gestire infrastrutture cloud con le migliori pratiche di DevOps. Imparerai a utilizzare strumenti come Docker e Kubernetes.</p>
        </div>
      </div>
      </section>
      <footer>
        <div className="footer-logo">
          <img src="src/img/logo.png" alt="Dev Labs logo" />
        </div>
      </footer>
    </div>
  );
}

export default HomePage;
