import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './Login.css';

function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const navigate = useNavigate();

  // gestione del login:
  function handleLogin(e) {
    e.preventDefault();
    try {
      fetch('http://localhost:8080/api/utente/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
      })
        .then(response => {
          if (response.ok) {
            // Utente loggato con successo
            // Controlla il ruolo dell'utente
            fetch('http://localhost:8080/api/ruolo/getall')
              .then(response => response.json())
              .then(data => {
                if (data.role === 'Admin') {
                  // L'utente è un admin, salva questa informazione nel localStorage
                  localStorage.setItem('isAdmin', true);
                }
                // Reindirizza alla pagina corretta
                navigate('/home');
              })
              .catch(error => {
                console.error('Error fetching user role:', error);
                setError('An unexpected error occurred.');
              });
          } else {
            return response.text().then(errorMessage => {
              setError(errorMessage);
            });
          }
        })
        .catch(error => {
          console.error('Error during login:', error);
          setError('An unexpected error occurred.');
        });
    } catch (error) {
      console.error('Error during login:', error);
      setError('An unexpected error occurred.');
    }
  }

  return (
    <div className="login-container">
      <div className = "login-form">
      <h2>Login</h2>
      {error && <div className="error-message">{error}</div>}
      <form onSubmit={handleLogin}>
        <input
          type="email"
          placeholder="Email address"
          className="input-field"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <input
          type="password"
          placeholder="Password"
          className="input-field"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
        />
        <button type="submit" className="login-button">
          Login
        </button>
      </form>
    </div>
    </div>
  );
}

export default Login;