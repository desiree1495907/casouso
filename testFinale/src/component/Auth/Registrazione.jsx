import React, { useState } from 'react';
import './Registrazione.css';

function Registrazione(){

    function handleChange(e) {
        const { name, value } = e.target;
        setFormData((prevData) => ({
          ...prevData,
          [name]: value,
        }));
      }
    
      async function handleSubmit(e) {
        e.preventDefault();
    
        try {
          const response = await fetch('http://localhost:8080/api/utente/registrazione', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(formData),
          });
    
          if (response.ok) {
            alert('Registrazione avvenuta con successo!');
            // Puoi fare qui il redirect a una pagina di successo o fare altre azioni necessarie
          } else {
            // Gestione degli errori, ad esempio mostrare un messaggio all'utente
            const errorData = await response.json();
            alert(`Errore durante la registrazione: ${errorData.message}`);
          }
        } catch (error) {
          console.error('Errore durante la richiesta di registrazione:', error);
        }
      }
    
      const [formData, setFormData] = useState({
        username: '',
        email: '',
        password: '',
      });
    
      return (
        <div className="registration-container">
          <h2>Registrazione</h2>
          <form onSubmit={handleSubmit}>
            <input
              type="text"
              name="username"
              placeholder="Username"
              value={formData.username}
              onChange={handleChange}
              required
            />
            <input
              type="email"
              name="email"
              placeholder="Email"
              value={formData.email}
              onChange={handleChange}
              required
            />
            <input
              type="password"
              name="password"
              placeholder="Password"
              value={formData.password}
              onChange={handleChange}
              required
            />
            <button type="submit">Registrati</button>
          </form>
        </div>
      );
    }
    
export default Registrazione;