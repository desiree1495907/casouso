import React from 'react'
import ReactDOM from 'react-dom/client'
//import App from './App.jsx'
//import './index.css'
import {createBrowserRouter, RouterProvider} from 'react-router-dom'
import HomePage from './component/HomePage/HomePage.jsx'
import Login from './component/Auth/Login.jsx'
import Registrazione from './component/Auth/Registrazione.jsx'
import HomeUser from './component/homeUser/HomeUser.jsx'

import '../node_modules/bootstrap/dist/css/bootstrap.css'
import '../node_modules/bootstrap/dist/js/bootstrap.bundle.js'

const router = createBrowserRouter ([
  {
    path: '/',
    element: <HomePage />
  },
  {
    path: '/login',
    element: <Login />
  },
  {
    path: '/register',
    element: <Registrazione />
  },
  {
    path: '/home',
    element: <HomeUser />
  }
])

ReactDOM.createRoot(document.getElementById('root')).render(
  <RouterProvider router={router}/>
)
