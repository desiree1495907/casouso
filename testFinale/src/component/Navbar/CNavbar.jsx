//import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import './CNavbar.css';


function CNavbar() {

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <a className="navbar-brand" href="/">Dev Labs</a>
      <ul className="navbar-nav">
        <li className="nav-item">
          <a className="nav-link" href="/login">Login</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="/register">Registrati</a>
        </li>
      </ul>
    </nav>
  );
}


export default CNavbar;
