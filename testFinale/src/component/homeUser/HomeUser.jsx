import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import './logout.css'

function HomeUser() {
  const [courses, setCourses] = useState([]);
  const [isAdmin, setIsAdmin] = useState(false);
  const [usersList, setUsersList] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    async function fetchData() {
      try {

        {/*
        // Recupera i corsi disponibili
        const coursesResponse = await fetch('http://localhost:8080/api/corso/getcorsi-da');
        if (coursesResponse.ok) {
          const coursesData = await coursesResponse.json();
          setCourses(coursesData);
        } else {
          console.error('Errore nel recupero dei corsi:', coursesResponse.statusText);
        }

        // Recupera i ruoli
        const roleResponse = await fetch('http://localhost:8080/api/ruolo/getall');
        if (roleResponse.ok) {
          const roleData = await roleResponse.json();
          setIsAdmin(roleData.role === 'Admin');
        } else {
          console.error('Errore nel recupero del ruolo utente:', roleResponse.statusText);
        }

    */}

        // Se l'utente è un admin, recupera la lista degli utenti
        const roleResponse = await fetch('http://localhost:8080/api/ruolo/getall');
        //console.log(roleResponse)
        if (roleResponse.status == 200) {
          const roleData = await roleResponse.json();
         // console.log(roleData)
            const ruoli = roleData.filter(ruolo => ruolo.tipologia == 'Admin')
            //console.log(ruoli)
          
  
          // Se l'utente è un admin, recupera la lista degli utenti
          if (ruoli.length != 0) {
            setIsAdmin(true);
            const usersResponse = await fetch('http://localhost:8080/api/utente/getUtenti');
            console.log(usersResponse)
            if (usersResponse.ok) {
              const usersData = await usersResponse.json();
              console.log(usersData)
              setUsersList(usersData);
            } else {
              console.error('Errore nel recupero della lista degli utenti:', usersResponse.statusText);
            }
          }
        } else {
          console.error('Errore nel recupero del ruolo utente:', roleResponse.statusText);
        }
      } catch (error) {
        console.error('Errore durante il recupero dei dati:', error);
      }
    }
  
    fetchData();
  }, []);



  // Funzione per effettuare il logout
  function handleLogout() {
    // Rimuove l'informazione di autenticazione dal localStorage
    localStorage.removeItem('token');
    localStorage.removeItem('isAdmin');

    // Reindirizza l'utente alla pagina di login 
    navigate('/login');
  }

  return (
    <div>
      <div className="logout-container">
        <button type="button" className="btn btn-secondary" onClick={handleLogout}>
          Logout
        </button>
      </div>

      <h2>Benvenuto !</h2>
      
      <h3>Corsi Disponibili:</h3>
      <div className="card-deck">
        {courses.map(course => (
          <div key={course.id} className="card" style={{ width: '18rem' }}>
            <div className="card-body">
              <h5 className="card-title">{course.title}</h5>
              <p className="card-text">{course.description}</p>
            </div>
          </div>
        ))}
      </div>

      {isAdmin && (
        <>
          <h3>Lista Utenti:</h3>
          <table className="table">
            <thead>
              <tr>
                <th>Nome</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
              {usersList.map(user => (
                <tr key={Math.random()}>
                  <td>{user.nome}</td>
                  <td>{user.email}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </>
      )}
    </div>
  );
}

export default HomeUser;